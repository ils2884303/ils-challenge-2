package com.ils.challenge.listeners;

import com.ils.challenge.model.SquirrelModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;

@Slf4j
public class CSVReadListener implements ItemReadListener<SquirrelModel> {
    @Override
    public void afterRead(SquirrelModel item) {
        log.info("Read item : " + item.getUnique_squirrel_id());
    }
}
