package com.ils.challenge.processor;

import com.ils.challenge.entity.Squirrel;
import com.ils.challenge.model.SquirrelModel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class SquirrelMapper implements ItemProcessor<Squirrel, SquirrelModel> {
    @Override
    public SquirrelModel process(@NonNull Squirrel item) {
        log.info("Processed item : " + item.getId());
        return SquirrelModel.builder()
                .x(item.getX())
                .y(item.getY())
                .unique_squirrel_id(item.getId())
                .hectare(item.getHectare())
                .shift(item.getShift())
                .date(item.getDate())
                .hectare_squirrel_number(item.getHectare_squirrel_number())
                .age(item.getAge())
                .primary_fur_color(item.getPrimary_fur_color())
                .highlight_fur_color(item.getHighlight_fur_color())
                .color_combination(item.getColor_combination())
                .color_notes(item.getColor_notes())
                .location(item.getLocation())
                .measurement(item.getMeasurement())
                .specific_location(item.getSpecific_location())
                .running(item.getRunning())
                .chasing(item.getChasing())
                .climbing(item.getClimbing())
                .eating(item.getEating())
                .foraging(item.getForaging())
                .other_activities(item.getOther_activities())
                .kuks(item.getKuks())
                .quaas(item.getQuaas())
                .moans(item.getMoans())
                .tail_flags(item.getTail_flags())
                .tail_twitches(item.getTail_twitches())
                .approaches(item.getApproaches())
                .indifferent(item.getIndifferent())
                .runs_from(item.getRuns_from())
                .other_interactions(item.getOther_interactions())
                .lat_long(item.getLat_long())
                .build();
    }
}