package com.ils.challenge.jobs;

import com.ils.challenge.entity.Squirrel;
import com.ils.challenge.listeners.CSVReadListener;
import com.ils.challenge.model.SquirrelModel;
import com.ils.challenge.processor.SquirrelMapper;
import com.ils.challenge.repository.SquirrelJPARepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JsonFileItemWriter;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class CvsToJson {

    private final static String[] FIELDS = {
            "x", "y", "unique_squirrel_id", "hectare", "shift", "date", "hectare_squirrel_number", "age",
            "primary_fur_color", "highlight_fur_color", "color_combination", "color_notes", "location", "measurement",
            "specific_location", "running", "chasing", "climbing", "eating", "foraging", "other_activities", "kuks",
            "quaas", "moans", "tail_flags", "tail_twitches", "approaches", "indifferent", "runs_from",
            "other_interactions", "lat_long"
    };

    private static final String INSERT_QUERY = """
            insert into squirrel (
            x, y, unique_squirrel_id, hectare, shift, date, hectare_squirrel_number, age, primary_fur_color,
            highlight_fur_color, color_combination, color_notes, location, measurement, specific_location, running, chasing,
            climbing, eating, foraging, other_activities, kuks, quaas, moans, tail_flags, tail_twitches, approaches,
            indifferent, runs_from, other_interactions, lat_long
            )
            values (:x, :y, :unique_squirrel_id, :hectare, :shift, :date, :hectare_squirrel_number, :age, :primary_fur_color,
            :highlight_fur_color, :color_combination, :color_notes, :location, :measurement, :specific_location, :running, :chasing,
            :climbing, :eating, :foraging, :other_activities, :kuks, :quaas, :moans, :tail_flags, :tail_twitches, :approaches,
            :indifferent, :runs_from, :other_interactions, :lat_long)""";

    private final JobRepository jobRepository;
    private final SquirrelJPARepository squirrelJPARepository;

    @Value("${input.file}")
    private String inputUrl;
    @Value("${output.file}")
    private String outputUrl;

    @Bean(name = "csvToJsonJob")
    public Job job(Step step1, Step step2) {
        var name = "Job #1";
        return new JobBuilder(name, jobRepository)
                .start(step1)
                .next(step2)
                .build();
    }

    // region step1
    @Bean
    public Step step1(ItemReader<SquirrelModel> csvFileReader,
                      ItemReadListener<SquirrelModel> readListener,
                      ItemWriter<SquirrelModel> sqliteItemWriter,
                      PlatformTransactionManager tM) {
        var name = "Step #1 Job #1";
        return new StepBuilder(name, jobRepository)
                .<SquirrelModel, SquirrelModel>chunk(5, tM)
                .reader(csvFileReader)
                .listener(readListener)
                .writer(sqliteItemWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<SquirrelModel> csvFileReader() {
        var itemReader = new FlatFileItemReader<SquirrelModel>();
        itemReader.setLineMapper(new DefaultLineMapper<>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setDelimiter(",");
                setNames(FIELDS);
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                setTargetType(SquirrelModel.class);
            }});
        }});
        itemReader.setResource(new ClassPathResource(inputUrl));
        return itemReader;
    }

    @Bean
    public ItemReadListener<SquirrelModel> readListener() {
        return new CSVReadListener();
    }

    @Bean
    public JdbcBatchItemWriter<SquirrelModel> sqliteItemWriter(DataSource dataSource) {
        var provider = new BeanPropertyItemSqlParameterSourceProvider<SquirrelModel>();
        var itemWriter = new JdbcBatchItemWriter<SquirrelModel>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql(INSERT_QUERY);
        itemWriter.setItemSqlParameterSourceProvider(provider);
        return itemWriter;
    }
    // endregion

    // region step2
    @Bean
    public Step step2(
            RepositoryItemReader<Squirrel> sqliteItemReader,
            ItemProcessor<Squirrel, SquirrelModel> processor,
            JsonFileItemWriter<SquirrelModel> jsonFileWriter,
            PlatformTransactionManager tM) {
        var name = "Step #2 Job #1";
        return new StepBuilder(name, jobRepository)
                .<Squirrel, SquirrelModel>chunk(5, tM)
                .reader(sqliteItemReader)
                .processor(processor)
                .writer(jsonFileWriter)
                .build();
    }

    @Bean
    public RepositoryItemReader<Squirrel> sqliteItemReader() {
        Map<String, Sort.Direction> sortMap = new HashMap<>();
        sortMap.put("id", Sort.Direction.DESC);

        return new RepositoryItemReaderBuilder<Squirrel>()
                .repository(squirrelJPARepository)
                .methodName("findAllBy")
                .sorts(sortMap)
                .pageSize(15)
                .saveState(false)
                .build();
    }

    @Bean
    public ItemProcessor<Squirrel, SquirrelModel> processor() {
        return new SquirrelMapper();
    }

    @Bean
    public JsonFileItemWriter<SquirrelModel> jsonFileWriter() {
        return new JsonFileItemWriterBuilder<SquirrelModel>()
                .name("jsonFileWriter")
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(outputUrl))
                .build();
    }
    // endregion
}
