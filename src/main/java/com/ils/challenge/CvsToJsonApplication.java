package com.ils.challenge;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@Slf4j
public class CvsToJsonApplication implements CommandLineRunner {

    private final JobLauncher jobLauncher;
    private final ApplicationContext applicationContext;

    public CvsToJsonApplication(JobLauncher jobLauncher, ApplicationContext applicationContext) {
        this.jobLauncher = jobLauncher;
        this.applicationContext = applicationContext;
    }

    public static void main(String[] args) {
        SpringApplication.run(CvsToJsonApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Job job = (Job) applicationContext.getBean("csvToJsonJob");

        JobParameters jobParameters = new JobParametersBuilder()
                .addString("JobID", "job-#-" + System.currentTimeMillis())
                .toJobParameters();

        var jobExecution = jobLauncher.run(job, jobParameters);

        log.info("" + jobExecution.getStatus());
    }
}
