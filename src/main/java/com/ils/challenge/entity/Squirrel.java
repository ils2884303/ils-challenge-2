package com.ils.challenge.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Table(name = "squirrel")
@Entity
public class Squirrel {
    private String x;
    private String y;

    @Id
    @Column(name = "unique_squirrel_id", unique = true, nullable = false)
    private String id;

    private String hectare;
    private String shift;
    private String date;
    private String hectare_squirrel_number;
    private String age;
    private String primary_fur_color;
    private String highlight_fur_color;
    private String color_combination;
    private String color_notes;
    private String location;
    private String measurement;
    private String specific_location;
    private String running;
    private String chasing;
    private String climbing;
    private String eating;
    private String foraging;
    private String other_activities;
    private String kuks;
    private String quaas;
    private String moans;
    private String tail_flags;
    private String tail_twitches;
    private String approaches;
    private String indifferent;
    private String runs_from;
    private String other_interactions;
    private String lat_long;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Squirrel that = (Squirrel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
