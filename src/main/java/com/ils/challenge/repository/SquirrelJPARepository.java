package com.ils.challenge.repository;

import com.ils.challenge.entity.Squirrel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SquirrelJPARepository extends JpaRepository<Squirrel, String> {
    Page<Squirrel> findAllBy(Pageable pageable);
}
