CREATE TABLE squirrel
(
    x                       text,
    y                       text,
    unique_squirrel_id      text,
    hectare                 text,
    shift                   text,
    date                    text,
    hectare_squirrel_number text,
    age                     text,
    primary_fur_color       text,
    highlight_fur_color     text,
    color_combination       text,
    color_notes             text,
    location                text,
    measurement             text,
    specific_location       text,
    running                 text,
    chasing                 text,
    climbing                text,
    eating                  text,
    foraging                text,
    other_activities        text,
    kuks                    text,
    quaas                   text,
    moans                   text,
    tail_flags              text,
    tail_twitches           text,
    approaches              text,
    indifferent             text,
    runs_from               text,
    other_interactions      text,
    lat_long                text
);